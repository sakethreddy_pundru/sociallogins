//
//  ViewController.m
//  GoogleDemo
//
//  Created by Saketh Reddy on 03/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import "SecondViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        SecondViewController *svc = [[SecondViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
    }
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileUpdated:) name:FBSDKProfileDidChangeNotification object:nil];
    
    

    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    if ([GIDSignIn sharedInstance].hasAuthInKeychain) {
        NSLog(@"Signed in");
//        [[GIDSignIn sharedInstance] signInSilently];
                    SecondViewController *svc = [[SecondViewController alloc]init];
                    [self.navigationController pushViewController:svc animated:YES];
        
    } else {
        NSLog(@"Not signed in");
    }

    


}

-(void)profileUpdated:(NSNotification *)note{
    
    if ([FBSDKAccessToken currentAccessToken] != nil) {
        
        NSString *name = [FBSDKProfile.currentProfile name];
        NSString *uid  = [FBSDKProfile.currentProfile userID];
        NSString *fname = [FBSDKProfile.currentProfile firstName];
        NSString *lname = [FBSDKProfile.currentProfile lastName];
        NSURL *pplink = [FBSDKProfile.currentProfile imageURLForPictureMode:FBSDKProfilePictureModeSquare size:CGSizeMake(400, 400)];
        
        NSLog(@"%@",name);
        NSLog(@"%@",uid);
        NSLog(@"%@",fname);
        NSLog(@"%@",lname);
        NSLog(@"%@",pplink);
        
        
    }
}


-(void)getEmail{
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"email" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user:%@", result);
         }
     }];
    
}







// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
//- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [myActivityIndicator stopAnimating];
//}

// Present a view that prompts the user to sign in with Google
//- (void)signIn:(GIDSignIn *)signIn
//presentViewController:(UIViewController *)viewController {
//    [self presentViewController:viewController animated:YES completion:nil];
//}
//
//// Dismiss the "Sign in with Google" view
//- (void)signIn:(GIDSignIn *)signIn
//dismissViewController:(UIViewController *)viewController {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)loginClick:(id)sender {
    
    if ([FBSDKAccessToken currentAccessToken]==nil) {
        FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc]init];
        [fbLogin logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                NSLog(@"Process Error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            }else{
                NSLog(@"Logged in");
                
                [self getEmail];
                SecondViewController *svc = [[SecondViewController alloc]init];
                [self.navigationController pushViewController:svc animated:YES];
                
            }
        }];
        
    }else{
        
        SecondViewController *svc = [[SecondViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
    }

    
}
@end
