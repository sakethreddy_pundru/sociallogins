//
//  ViewController.h
//  GoogleDemo
//
//  Created by Saketh Reddy on 03/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@interface ViewController : UIViewController <GIDSignInUIDelegate>

@property (strong, nonatomic) IBOutlet GIDSignInButton *signInButton;

- (IBAction)loginClick:(id)sender;
@end

