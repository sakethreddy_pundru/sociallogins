//
//  SecondViewController.m
//  GoogleDemo
//
//  Created by Saketh Reddy on 03/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import "SecondViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.navigationItem.hidesBackButton = true;
    
    UIButton *myLogoutButton=[UIButton buttonWithType:UIButtonTypeCustom];
    myLogoutButton.backgroundColor=[UIColor darkGrayColor];
    myLogoutButton.frame=CGRectMake(0,0,180,40);
    myLogoutButton.center = self.view.center;
    [myLogoutButton setTitle: @"My Logout Button" forState: UIControlStateNormal];
    
    // Handle clicks on the button
    [myLogoutButton addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:myLogoutButton];
}

-(void)logoutButtonClicked{
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    if ( [FBSDKAccessToken currentAccessToken] ){
        [login logOut];
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
    }
    
    [[GIDSignIn sharedInstance] signOut];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
